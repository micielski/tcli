struct raw_request_result {
    char *response_body;
    char *headers;
};
void free_raw_request_result(struct raw_request_result*);

struct raw_request_result *raw_transmission_request(const char *url, const char *session_id, const char *body);
char *get_session_id(const char *url);
