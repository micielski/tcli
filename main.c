#include <stdlib.h>
#include <string.h>
#include <curl/curl.h>
#include <jansson.h>
#include "request.h"

#define URL "http://192.168.1.2:9091/transmission/rpc"
#define URL_SIZE 256

void transmission_get_info() {
    char* session_id = get_session_id(URL);
    printf("This is my session_id: %s", session_id);
    json_t *arguments = json_object();
    json_t *fields = json_array();
    json_array_append_new(fields, json_string("version"));
    json_object_set_new(arguments, "fields", fields);
    json_t *json_obj = json_object();
    json_object_set_new(json_obj, "arguments", arguments);
    json_object_set_new(json_obj, "method", json_string("session-get"));
    char *json_str = json_dumps(json_obj, JSON_INDENT(2));
    fflush(stdout);
    printf("%s\n", json_str);
    
    struct raw_request_result *res = raw_transmission_request(URL, session_id, json_str);

    json_decref(json_obj);
    free(json_str);

    puts(res->response_body);
    fflush(stdout);
    free_raw_request_result(res);
    free(session_id);
}

int main(int argc, char *argv[]) {
    size_t i;
    char *text;
    char url[URL_SIZE];

    json_t *root;
    json_error_t error;

    curl_global_init(CURL_GLOBAL_ALL);

    if (argc != 2) {
        fprintf(stderr, "usage: %s COMMAND\n\n", argv[0]);
        fprintf(stderr, "Interact with the Transmission daemon.\n\n");
        return 2;
    }

    if (strcmp("info", argv[1]) == 0) {
        transmission_get_info();
    }

    curl_global_cleanup();
    return 0;
}
