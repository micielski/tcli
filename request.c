#include <stdlib.h>
#include <string.h>

#include <curl/curl.h>

#include "request.h"

#define BUFFER_SIZE (256 * 1024) /* 256 KB */
#define SESSION_ID_HEADER_NAME "X-Transmission-Session-Id: "
#define MAX_HEADER_LENGTH 64

void free_raw_request_result(struct raw_request_result* res) {
    free(res->response_body);
    free(res->headers);
    free(res);
}

struct write_result {
    char *data;
    int pos;
};

static size_t write_data(void *buffer, size_t size, size_t nmemb, void *ctx) {
    struct write_result *result = (struct write_result *)ctx;

    if (result->pos + size * nmemb >= BUFFER_SIZE - 1) {
        fprintf(stderr, "error: too small buffer\n");
        return 0;
    }

    memcpy(result->data + result->pos, buffer, size * nmemb);
    result->pos += size * nmemb;

    return size * nmemb;
}

static char *get_session_id_from_headers(const char* headers) {
    char *header;
    char *res = strstr(headers, SESSION_ID_HEADER_NAME);

    if (res == NULL) {
        fprintf(stderr, "error: couldn't find session_id");
        exit(1);
    }

    res += strlen(SESSION_ID_HEADER_NAME);

    header = malloc(MAX_HEADER_LENGTH + 1);
    if (header == NULL) {
        fprintf(stderr, "error: malloc failed\n");
        exit(1);
    }

    size_t length = strcspn(res, "\n");
    if (length >= MAX_HEADER_LENGTH) {
        fprintf(stderr, "error: header exceeds maximum length (%zu)\n", length);
        free(header);
        exit(1);
    }

    strncpy(header, res, length);
    header[length-1] = '\0';
    
    return header;
}

static struct raw_request_result* raw_post_request(const char *url, struct curl_slist *headers, const char* body) {
    CURL *curl = NULL;
    CURLcode status;
    char *data = NULL;
    char *header_data = NULL;
    long code;

    curl = curl_easy_init();

    if (!curl)
        goto error;

    data = malloc(BUFFER_SIZE);
    if (!data)
        goto error;

    header_data = malloc(BUFFER_SIZE);
    if (!header_data)
        goto error;

    struct write_result write_result = {.data = data, .pos = 0};
    struct write_result write_header_result = {.data = header_data, .pos = 0};

    curl_easy_setopt(curl, CURLOPT_URL, url);

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &write_result);

    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, write_data);
    curl_easy_setopt(curl, CURLOPT_HEADERDATA, &write_header_result);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

    if (body != NULL) {
        curl_easy_setopt(curl, CURLOPT_POST, 1);
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS, body);
    }

    status = curl_easy_perform(curl);
    if (status != 0) {
        fprintf(stderr, "error: unable to request data from %s:\n", url);
        fprintf(stderr, "%s\n", curl_easy_strerror(status));
        goto error;
    }

    data[write_result.pos] = '\0';
    header_data[write_header_result.pos] = '\0';

    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &code);
    curl_easy_cleanup(curl);
    curl_slist_free_all(headers);

    struct raw_request_result *result = malloc(sizeof(struct raw_request_result));
    if (!result)
        goto error;

    result->response_body = data;
    result->headers = header_data;

    return result;

error:
    if (data)
        free(data);
    if (curl)
        curl_easy_cleanup(curl);
    if (headers)
        curl_slist_free_all(headers);
    return NULL;
}

char *get_session_id(const char *url) {
    struct raw_request_result *result = raw_post_request(url, NULL, NULL);
    char* session_id = get_session_id_from_headers(result->headers);
    free_raw_request_result(result);
    return session_id;
}

struct raw_request_result *raw_transmission_request(const char* url, const char* session_id, const char* body) {
    struct curl_slist *headers = NULL;
    struct curl_slist *temp_headers = NULL;

    headers = curl_slist_append(headers, "Content-Type: application/json");

    if (!headers) {
        fprintf(stderr, "error: unable to add a header");
        exit(1);
    }

    char* session_header = malloc(strlen(SESSION_ID_HEADER_NAME) + strlen(session_id) + 1);
    strcpy(session_header, SESSION_ID_HEADER_NAME);
    strcat(session_header, session_id);

    temp_headers = curl_slist_append(headers, session_header);
    if (!temp_headers) {
         fprintf(stderr, "error: unable to add header");
         exit(1);   
    }
    
    headers = temp_headers;
    
    struct raw_request_result *response = raw_post_request(url, headers, body);
    free(session_header);
    return response;
}
